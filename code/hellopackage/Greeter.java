package hellopackage;
import java.util.Scanner;
import java.util.Random;
import secondpackage.Utilities;
public class Greeter
{

    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        Random rand = new Random();
        System.out.println("Please enter an integer");
        int integ = scan.nextInt();
        Utilities util = new Utilities();
        System.out.println(util.doubleMe(integ));
    }
}
